package ch.tbz;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WeatherAPI {
    private LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
    private final String apiKey = "2564eb5846036972b50689e977bb3864";
    private DecimalFormat df = new DecimalFormat("0.00");


    public void getFromAPI(String place) {
        try {
            // get request
            URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=" + place + "&appid=" + apiKey); // Zurich
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");


            // gets the response as String

            // with stream buffer get response output
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();

            // reads line by line and adds it to string buffer
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            ObjectMapper mapper = new ObjectMapper();
            // parse String to Map
            responseMap = mapper.readValue(content.toString(), LinkedHashMap.class);


        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void showAPIInfo(String place) {
        try {
            var weather = (List<Map>) responseMap.get("weather");
            var temp = (Map<String, Double>) responseMap.get("main");
            System.out.println("weather in " + place + " is: " + weather.get(0).get("description") + "\nTemp is: " + df.format(temp.get("temp") - 273.15) + "°C");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public String getWeather() {
        var list = (List<Map>) responseMap.get("weather");
        return (String) list.get(0).get("description");
    }
}