package ch.tbz;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class History {
    File file;
    LinkedHashMap<String, String> weather = new LinkedHashMap<>();


    public History() {
        readFile();
    }

    private void readFile() {
        try {
            file = new File("history.json");
            Scanner s = new Scanner(file);
            String scoreString = "";
            while (s.hasNextLine()) {
                scoreString += s.nextLine();
            }
            ObjectMapper mapper = new ObjectMapper();
            // parse String to Map
            weather = mapper.readValue(scoreString.toString(), LinkedHashMap.class);
            s.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void printWeather() {
        for (Map.Entry<String, String> entry : weather.entrySet()) {
            System.out.println("Place: " + entry.getKey() + "   Weather was: " + entry.getValue());
        }
    }

    public void addNewWeather(String place, String weather) {
        this.weather.put(place, weather);

    }

    public void saveWeather() {
        try {
            FileWriter fileWriter = new FileWriter("history.json");
            String writeTo = "{";

            for (Map.Entry<String, String> entry : weather.entrySet()) {
                writeTo += "\"" + entry.getKey() + "\":\"" + entry.getValue() + "\",";
            }
            writeTo = writeTo.substring(0, writeTo.length() - 1);
            writeTo += "}";
            fileWriter.write(writeTo);
            fileWriter.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
