package ch.tbz;

import java.util.Scanner;

public class Menu {

    private boolean exit = false;
    private WeatherAPI weatherAPI = new WeatherAPI();
    private History history = new History();

    public void mainMenu() {
        Scanner s = new Scanner(System.in);
        try {
            while (!exit) {
                System.out.println("********************************************\nWelcome to the weather please enter place where you want the weather from:\n1. Zurich\n2. Bern\n3. show history\n4. Quit");
                chooseOption(s.nextInt());
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            s.close();
        }
    }

    private void chooseOption(int choosen) {

        switch (choosen) {
            case 1:
                // Zurich
                weatherAPI.getFromAPI(City.zurich);
                weatherAPI.showAPIInfo(City.zurich);
                history.addNewWeather(City.zurich, weatherAPI.getWeather());
                break;

            case 2:
                // Bern
                weatherAPI.getFromAPI(City.bern);
                weatherAPI.showAPIInfo(City.bern);
                history.addNewWeather(City.bern, weatherAPI.getWeather());
                break;
            case 3:
                history.printWeather();
                break;
            case 4:
                history.saveWeather();
                exit = true;
                break;
            default:
                System.out.println("wrong input");
        }
    }
}
